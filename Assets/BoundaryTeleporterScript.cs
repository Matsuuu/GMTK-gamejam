﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundaryTeleporterScript : MonoBehaviour {

    public enum BoundarySide { Left, Right, Up, Down};

    public BoundarySide Side;
    public GameObject player;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    private void OnTriggerEnter2D(Collider2D collision)
    {
        player = collision.gameObject;
        switch(Side)
        {
            case BoundarySide.Left:
                handleLeftSideTrigger();
                break;
            case BoundarySide.Right:
                handleRightSideTrigger();
                break;
            case BoundarySide.Up:
                handleUpSideTrigger();
                break;
            case BoundarySide.Down:
                handleDownSideTrigger();
                break;
            default:

                break;
        }
    }

    void handleLeftSideTrigger()
    {
        GameObject oppositePair = GameObject.Find("BoundaryTrigger-Right");
        Vector3 playerPos = player.transform.position;
        player.transform.position = new Vector3(oppositePair.transform.position.x - 1, playerPos.y, playerPos.z);
    }

    void handleRightSideTrigger()
    {
        GameObject oppositePair = GameObject.Find("BoundaryTrigger-Left");
        Vector3 playerPos = player.transform.position;
        player.transform.position = new Vector3(oppositePair.transform.position.x + 1, playerPos.y, playerPos.z);
    }

    void handleUpSideTrigger()
    {
        GameObject oppositePair = GameObject.Find("BoundaryTrigger-Down");
        Vector3 playerPos = player.transform.position;
        player.transform.position = new Vector3(playerPos.x, oppositePair.transform.position.y + 1, playerPos.z);
    }

    void handleDownSideTrigger()
    {
        GameObject oppositePair = GameObject.Find("BoundaryTrigger-Up");
        Vector3 playerPos = player.transform.position;
        player.transform.position = new Vector3(playerPos.x, oppositePair.transform.position.y - 1, playerPos.z);
    }


}
