﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ComboCounterScript : MonoBehaviour {

    public int count; 
    private Text comboCounterText;
    private GameObject explosion;
    public int neededCombo;
    public float dropOffTime;
    private DifficultyScript ds;

    public int score;

    // Use this for initialization
    void Start ()
    {
        score = 1000;
        if (GameObject.Find("DifficultyCarrier") != null)
        {
            ds = GameObject.Find("DifficultyCarrier").GetComponent<DifficultyScript>();
            getDropTimeByDiff();
            neededCombo = getNeededComboByDiff();
        } else
        {
            neededCombo = 10;
        }
        comboCounterText = GetComponentInChildren<Text>();
        count = 0;
        explosion = (GameObject) Resources.Load("Prefabs/HitEffect");
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public int getNeededComboByDiff()
    {
        int c;
        switch(ds.chosenDifficulty)
        {
            case "Normal":
                c = 10;
                break;
            case "Hard":
                c = 15;
                break;
            case "Extreme":
                c = 15;
                break;
            default:
                c = 10;
                break;
        }
        return c;
    }

    public void getDropTimeByDiff()
    {
        switch (ds.chosenDifficulty)
        {
            case "Normal":
                dropOffTime = 1;
                break;
            case "Hard":
                dropOffTime = 0.5f;
                break;
            case "Extreme":
                dropOffTime = 0;
                break;
            default:
                dropOffTime = 2;
                break;
        }
    }

    public void updateCounter()
    {
        CancelInvoke("resetCombo");
        count++;
        comboCounterText.text = count.ToString();
        if (count >= neededCombo)
        {
            Time.timeScale = 0.01f;
            GameObject camera = GameObject.FindGameObjectWithTag("MainCamera");
            GameObject player = GameObject.Find("Player");
            camera.transform.position = new Vector3(player.transform.position.x, player.transform.position.y, -10);
            camera.GetComponent<Camera>().orthographicSize = 2;
            StartCoroutine(startExplosions(player.transform.position, player.transform.rotation));
        }
    }

    IEnumerator startExplosions(Vector3 position, Quaternion rotation)
    {
        GameObject camera = GameObject.FindGameObjectWithTag("MainCamera");
        GameObject player = GameObject.Find("Player");
        GameObject.Find("ScoreBoard").GetComponentInChildren<Text>().text = "Score: " + score;
        GameObject.Find("ScoreBoard").GetComponentInChildren<Text>().enabled = true;
        for (int i = 0; i <= 60; i++)
        {
            camera.transform.position = new Vector3(player.transform.position.x, player.transform.position.y, -10);
            camera.GetComponent<Camera>().orthographicSize = 2;
            Instantiate(explosion, new Vector3(position.x + Random.Range(-4.0f, 4.0f), position.y + Random.Range(-4.0f, 4.0f), 1), rotation);
            yield return new WaitForSecondsRealtime(0.1f);
        }
        Time.timeScale = 0;
        SceneManager.LoadScene("MenuScene");
    }

    void backToMenu()
    {
    }

    public void startCounter()
    {
        Invoke("resetCombo", dropOffTime);
    }

    public void handleScore(int s)
    {
        score = score - s;
    }

    void resetCombo()
    {
        count = 0;
        comboCounterText.text = "";
        GameObject.Find("Dummy").GetComponent<SandBagScript>().resetCounters();
        score = 1000;
    }
}
