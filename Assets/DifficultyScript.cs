﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DifficultyScript : MonoBehaviour {

    public string chosenDifficulty;
	// Use this for initialization
	void Start () {
        DontDestroyOnLoad(gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void choiceDifficulty(string diff)
    {
        chosenDifficulty = diff;
    }
}
