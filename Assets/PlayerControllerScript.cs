﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerScript : MonoBehaviour {

    Rigidbody2D rigidbody;
    PolygonCollider2D collider;
    Animator animator;

    private GameObject hitEffect;

    public bool canMove;

    public int maxHorizVelocity;
    public int maxVertVelocity;

    public int maxJumps;
    public int usedJumps;
    public int jumpPower;

    public int fastFallTrigger;
    public bool isFastFalling;
    public bool isListeningForTap;
    
    public bool dropThroughIsActive;

    public bool isOnPlatform;
    public GameObject platForm;

    private ComboCounterScript CCS;
    private CameraShakerScript CSS;

	// Use this for initialization
	void Start () {
        Time.timeScale = 1;
        rigidbody = GetComponent<Rigidbody2D>();
        collider = GetComponent<PolygonCollider2D>();
        animator = GetComponent<Animator>();
        platForm = GameObject.Find("Laatta");
        CCS = GameObject.Find("ComboCounter").GetComponent<ComboCounterScript>();
        CSS = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraShakerScript>();
        rigidbody.freezeRotation = true;
        animator.SetBool("IsFacingRight", true);
        isOnPlatform = true;
        hitEffect = (GameObject) Resources.Load("Prefabs/Hiteffect");
        ignorePlayerCollisions();
	}
	
	// Update is called once per frame
	void Update () {
        if (isAttackKey())
            handleAttack();
        if (isJumpKey())
            jump();
    }

    private void FixedUpdate()
    {
        handleInput();
    }

    private void ignorePlayerCollisions()
    {
        foreach (GameObject p in GameObject.FindGameObjectsWithTag("Player")) {
            Physics2D.IgnoreCollision(collider, p.GetComponent<PolygonCollider2D>());
        }
        foreach (GameObject p in GameObject.FindGameObjectsWithTag("Sandbag"))
        {
            Physics2D.IgnoreCollision(collider, p.GetComponent<PolygonCollider2D>());
        }
    }

    void handleInput()
    {
        
        if (isMovementKey())
        {
            movePlayer();
        } else
        {
            if (isOnPlatform)
            {
                rigidbody.velocity = new Vector3(rigidbody.velocity.x / 2, rigidbody.velocity.y);
                animator.SetBool("Moving", false);
            }
        }
        if (Input.GetAxis("Vertical") == -1)
        {
            dropThrough();
            if (!isOnPlatform)
            {
                handleFastFall();
            }
        }
    }

    void movePlayer()
    {
        string facingDirection;
        
        facingDirection = Input.GetAxis("Horizontal") > 0 ? "Right" : "Left";
        animator.SetBool(facingDirection == "Right" ? "IsFacingRight" : "IsFacingLeft", true);
        animator.SetBool(facingDirection == "Right" ? "IsFacingLeft" : "IsFacingRight", false);
        animator.SetBool("Moving", true);
        if (isOnPlatform)
        {
            handlePlatformMovement();
        } else
        {
            handleAerialMovement();
        }
    }

    void handlePlatformMovement()
    {
        float vertAxis, horizAxis;
        horizAxis = getHorizontalPlatformMovement();
        rigidbody.AddForce(new Vector2(horizAxis, 0) * 12);
        if (rigidbody.velocity.x < 2 && Input.GetAxis("Horizontal") > 0)
            rigidbody.velocity = new Vector2(2, rigidbody.velocity.y);
        if (rigidbody.velocity.x > -2 && Input.GetAxis("Horizontal") < 0)
            rigidbody.velocity = new Vector2(-2, rigidbody.velocity.y);
    }

    void handleAerialMovement()
    {
        float vertAxis;
        vertAxis = Input.GetAxis("Vertical");
        if (vertAxis < 0)
        {
            if (isFastFalling)
            {
                rigidbody.AddForce(new Vector2(Input.GetAxis("Horizontal") / 2, vertAxis) * 10);
            } else
            {
                rigidbody.AddForce(new Vector2(Input.GetAxis("Horizontal"), 0) * 10);
            }
        }
        else
        {
            rigidbody.AddForce(new Vector2(Input.GetAxis("Horizontal"), 0) * 10);
        }

        float verticalVelocity = rigidbody.velocity.y;
        float horizontalVelocity = rigidbody.velocity.x;

        if (rigidbody.velocity.y < -7 && rigidbody.velocity.y < maxVertVelocity * (-1))
        {
            verticalVelocity = maxVertVelocity * (-1);
        }
        if (rigidbody.velocity.x > maxHorizVelocity)
        {
            horizontalVelocity = maxHorizVelocity;
        } else if (rigidbody.velocity.x < maxHorizVelocity * (-1))
        {
            horizontalVelocity = maxHorizVelocity * (-1);
        }
        rigidbody.velocity = new Vector2(horizontalVelocity, verticalVelocity);
    }

    float getHorizontalPlatformMovement()
    {
        float horizAxis = 0;
        if (Input.GetAxis("Horizontal") > 0 && rigidbody.velocity.x < maxHorizVelocity)
        {
            horizAxis = Input.GetAxis("Horizontal");
        } else if (Input.GetAxis("Horizontal") < 0 && rigidbody.velocity.x > maxHorizVelocity * (-1))
        {
            horizAxis = Input.GetAxis("Horizontal");
        }
        return horizAxis;
    }
    


    void jump()
    {
        if (usedJumps < maxJumps)
        {
            usedJumps++;
            animator.ResetTrigger("Landing");
            StartCoroutine(doJump());
        }
    }

    IEnumerator doJump()
    {
        yield return new WaitForSeconds(0.05f);
        animator.SetTrigger("Jump");
        if (Input.GetButton("Jump"))
        {
            rigidbody.velocity = new Vector2(rigidbody.velocity.x, 0);
            rigidbody.AddForce(new Vector2(0, 1) * jumpPower);
        }
        else
        {
            rigidbody.velocity = new Vector2(rigidbody.velocity.x, 0);
            rigidbody.AddForce(new Vector2(0, 1) * jumpPower * 0.5f);
        }
    }

    void handleFastFall()
    {
        fastFallTrigger++;
        if (fastFallTrigger >= 2)
        {
            isFastFalling = true;
        }
    }

    void dropThrough()
    {
        if (!isListeningForTap)
        {
            isListeningForTap = true;
            StartCoroutine(listenForAnotherTap());
        }
    }

    IEnumerator listenForAnotherTap()
    {
        int i = 0;
        bool released = false;
        while (i < 60)
        {
            if (Input.GetAxis("Vertical") > -1 && !released)
            {
                released = true;
                Debug.Log("Released");
            }
            if (released && Input.GetAxis("Vertical") == -1)
            {
                Debug.Log("Ebin");
                dropThroughIsActive = true;
                rigidbody.velocity = new Vector2(rigidbody.velocity.x, -10);
                Physics2D.IgnoreCollision(collider, platForm.GetComponent<BoxCollider2D>());
                StartCoroutine(returnCollision());
                i = 61;
            }
            i++;
            yield return new WaitForFixedUpdate();
        }
        if (i == 60)
        {
            isListeningForTap = false;
        }
    }

    IEnumerator returnCollision()
    {
        yield return new WaitForSeconds(0.05f);
        rigidbody.velocity = new Vector2(rigidbody.velocity.x, -7.5f);
        isOnPlatform = false;
        animator.SetTrigger("Drop");
        yield return new WaitForSeconds(0.2f);
        Physics2D.IgnoreCollision(collider, platForm.GetComponent<BoxCollider2D>(), false);
        dropThroughIsActive = false;
        isListeningForTap = false;
    }


    void handleAttack()
    {
        float verticalAxis = Input.GetAxis("Vertical");
        float horizontalAxis = Input.GetAxis("Horizontal");
        string heading = "none";
        if (verticalAxis >= 0 && horizontalAxis >= 0)
        {
            if (horizontalAxis == 0 && verticalAxis == 0)
            {
                heading = "none";
            }
            else
            {
                heading = verticalAxis > horizontalAxis ? "Up" : "Right";
            }
        } else if (verticalAxis >= 0 && horizontalAxis <= 0)
        {
            heading = verticalAxis > horizontalAxis * (-1) ? "Up" : "Left";
        } else if (verticalAxis <= 0 && horizontalAxis >= 0)
        {
            heading = verticalAxis * (-1) > horizontalAxis ? "Down" : "Right";
        } else if (verticalAxis <= 0 && horizontalAxis <= 0)
        {
            heading = verticalAxis < horizontalAxis ? "Down" : "Left";
        }
        
        switch(heading)
        {
            case "Up":
                handleUpAttack();
                break;
            case "Down":
                handleDownAttack();
                break;
            case "Left":
                handleLeftAttack();
                break;
            case "Right":
                handleRightAttack();
                break;
            default:
                handleJab();
                break;
        }
    }

    void handleUpAttack()
    {
        if (isOnPlatform)
        {
            animator.SetTrigger("Utilt");
        } else
        {
            animator.SetTrigger("Uair");
        }
    }

    void handleDownAttack()
    {
        if (isOnPlatform)
        {
            animator.SetTrigger("Dtilt");
        }
        else
        {
            animator.SetTrigger("Dair");
        }
    }

    void handleLeftAttack()
    {
        if (isOnPlatform)
        {
            animator.SetTrigger("Ftilt");
        }
        else
        {
            animator.SetTrigger("Fair");
        }
    }

    void handleRightAttack()
    {
        if (isOnPlatform)
        {
            animator.SetTrigger("Ftilt");
        }
        else
        {
            animator.SetTrigger("Fair");
        }
    }

    void handleJab()
    {
        if (isOnPlatform)
        {
            animator.SetTrigger("Jab");
        }
        else
        {
            animator.SetTrigger("Nair");
        }
    }


    bool isMovementKey()
    {
        return Input.GetAxis("Horizontal") != 0;
    }

    bool isJumpKey()
    {
        return Input.GetButtonDown("Jump");
    }

    bool isAttackKey()
    {
        return Input.GetButtonDown("Fire1");
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Platform")
        {
            if (!isOnPlatform)
            {
                animator.SetTrigger("Landing");

                animator.ResetTrigger("Uair");
                animator.ResetTrigger("Fair");
                animator.ResetTrigger("Nair");
                animator.ResetTrigger("Dair");
            }
            usedJumps = 0;
            isFastFalling = false;
            fastFallTrigger = 0;
            rigidbody.velocity = new Vector2(rigidbody.velocity.x / 1.5f, rigidbody.velocity.y / 2);
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Platform")
        {
            isOnPlatform = true;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Platform")
        {
            isOnPlatform = false;
        }
    }

}
