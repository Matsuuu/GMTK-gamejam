﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByTime : MonoBehaviour {

    public float time;

	// Use this for initialization
	void Start () {
        Invoke("destroySelf", time);	
	}

    void destroySelf()
    {
        Destroy(gameObject);
    }
	
}
