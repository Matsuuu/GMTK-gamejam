﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SandBagScript : MonoBehaviour {

    ComboCounterScript CCS;
    Rigidbody2D rigidBody;
    public bool onGround;
    int stomp = 0, jab = 0, megakick = 0, knee = 0, headbutt = 0, tongue = 0, backflip = 0, sweep = 0;
    string lastAttack;

    // Use this for initialization
    void Start () {
        onGround = true;
        rigidBody = GetComponent<Rigidbody2D>();
        CCS = GameObject.Find("ComboCounter").GetComponent<ComboCounterScript>();
	}
	
	// Update is called once per frame
	void Update () {
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Platform")
        {
            CCS.startCounter();
        } else
        {
            if (collision.gameObject.GetComponent<HitBoxScript>().hitName == lastAttack)
            {
                CCS.handleScore(100);
            }
            switch (collision.transform.name)
            {
                case "Stomp":
                    stomp++;
                    break;
                case "Jab":
                    jab++;
                    break;
                case "MegaKick":
                    megakick++;
                    break;
                case "Knee":
                    knee++;
                    break;
                case "Headbutt":
                    headbutt++;
                    break;
                case "Tongue":
                    tongue++;
                    break;
                case "Backflip":
                    backflip++;
                    break;
                case "Sweep":
                    sweep++;
                    break;
                default:
                    break;
            }
            lastAttack = collision.gameObject.GetComponent<HitBoxScript>().hitName;
        }
    }

    public void resetCounters()
    {
        stomp = jab = megakick = knee = headbutt = tongue = backflip = sweep = 0;
        lastAttack = "";
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Platform")
        {
            onGround = true;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Platform")
        {
            onGround = false;
        }
    }
}
