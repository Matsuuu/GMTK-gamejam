﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitBoxScript : MonoBehaviour {

    public int forceX = 4;
    public int forceY = 10;
    public int forcePower = 40;

    private GameObject HitEffect;
    private bool hasHit;
    private CameraShakerScript CSS;
    private ComboCounterScript CCS;

    public string hitName;

    private GameObject sandBag;

    private void Start()
    {
        Physics2D.IgnoreCollision(GetComponent<Collider2D>(), transform.parent.parent.parent.parent.GetComponent<PolygonCollider2D>());
        HitEffect = (GameObject)Resources.Load("Prefabs/Hiteffect");
        CSS = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraShakerScript>();
        CCS = GameObject.Find("ComboCounter").GetComponent<ComboCounterScript>();
        sandBag = GameObject.Find("Dummy");
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        
        if ((collision.gameObject.tag == "Sandbag" || collision.gameObject.tag == "Player") && collision.gameObject != transform.parent.parent.parent.parent && !hasHit)
        {
            hasHit = true;
            Instantiate(HitEffect, transform.position, transform.rotation);
            CSS.shakeCamera();
            Invoke("reset", 0.3f);
            knockBackTarget(collision);
            handleCombo();
        }
    }

    void handleCombo()
    {
        CCS.updateCounter();
    }

    void knockBackTarget(Collision2D collision)
    {
        if (sandBag.GetComponent<SandBagScript>().onGround)
            StartCoroutine(ignoreForAWhile(collision));
        ContactPoint2D col = collision.contacts[0];
        collision.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(col.point.x / forceX, col.point.y + forceY) * forcePower);
    }

    IEnumerator ignoreForAWhile(Collision2D collision)
    {
        Physics2D.IgnoreCollision(GetComponent<Collider2D>(), collision.gameObject.GetComponent<Collider2D>());
        yield return new WaitForSeconds(0.4f);
        Physics2D.IgnoreCollision(GetComponent<Collider2D>(), collision.gameObject.GetComponent<Collider2D>(), false);
    }

    private void reset()
    {
        hasHit = false;
    }

}
    