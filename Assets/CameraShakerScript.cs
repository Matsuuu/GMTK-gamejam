﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShakerScript : MonoBehaviour {

    Vector3 originalPosition;

	// Use this for initialization
	void Start () {
        originalPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void shakeCamera()
    {
        List<Vector3> shakePositions = new List<Vector3>();
        for (int i = 0; i <= 3; i++)
        {
            shakePositions.Add(new Vector3(Random.Range(-0.1f, 0.1f), Random.Range(-0.1f, 0.1f), originalPosition.z));
        }
        StartCoroutine(shakeTheScreen(shakePositions));
    }

    IEnumerator shakeTheScreen(List<Vector3> shakePositions)
    {
        foreach(Vector3 pos in shakePositions)
        {
            transform.position = pos;
            yield return new WaitForSeconds(0.05f);
        }
        transform.position = originalPosition;
    }
}
