﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    DifficultyScript ds;

    private void Start()
    {
        ds = GameObject.Find("DifficultyCarrier").GetComponent<DifficultyScript>();
    }

    public void GameSceneNormal()
    {
        Debug.Log("Load GameScene");
        ds.choiceDifficulty("Normal");
        SceneManager.LoadScene("GameScene");
    }

    public void GameSceneHard()
    {
        Debug.Log("Load GameScene");
        ds.choiceDifficulty("Hard");
        SceneManager.LoadScene("GameScene");
    }

    public void GameSceneExtreme()
    {
        Debug.Log("Load GameScene");
        ds.choiceDifficulty("Extreme");
        SceneManager.LoadScene("GameScene");
    }

    public void CreditsScene()
    {
        Debug.Log("Load CreditsScene");
        SceneManager.LoadScene("CreditsScene");
    }

    public void MenuScene()
    {
        Debug.Log("Load MenuScene");
        SceneManager.LoadScene("MenuScene");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}